const User = require('../models/user.model');

 
function isAdministrator(username) {
    const users = User.users;
    const userIndex = users.findIndex(u => u.username === username);
    
    const isAdmin_value = users[userIndex]['isAdmin'];
    
    if(isAdmin_value) {
        return true;
        next();
    } else {
        return false;
    }
}



module.exports = isAdministrator;
