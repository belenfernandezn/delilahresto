const express = require('express');
const router = express.Router();
const User = require('../models/user.model');


/**
 * @swagger
 * /user/{id}:
 *  get:
 *      summary: Get single user by id
 *      tags: [User]
 *      parameters:
 *          - in: path
 *            name: id
 *            schema:
 *              type: integer
 *              required: true
 *              description: User's id
 *      responses:
 *          200:
 *              description: User info
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/User'
 *          401:
 *              description: User must be logged in.
 */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    const user = User.getUserById(id);
    if (user) {
        res.status(200).json(user);
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "No existe un usuario con ese id."
            }
        );
    }
});


/**
 *@swagger
 * /signup:
 *  post:
 *      summary: Create new user
 *      tags: [User]
 *      security: []
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                     $ref: '#/components/schemas/User'
 *      responses:
 *          200:
 *              description: The user was created
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          401:
 *              description: Username and password are required
 */
 router.post('/signup', (req,res) => {
    const user = req.body;
    new_user = User.createNewUser(user);

    const id = user.id;
    const user_exists = User.getUserById(id);

    if (user_exists) {
        res.status(200).json(
            {
                status: true,
                msg: "El usuario no ha sido creado."
            }
        );
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "Ocurrió un error. El usuario no ha sido creado."
            }
        );
    } 
});

/**
 *@swagger
 * /login:
 *  post:
 *      summary: Log in user
 *      tags: [User]
 *      security: []
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                      $ref: '#/components/schemas/User'
 *      responses:
 *          200:
 *              description: User is logged in
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          401:
 *              description: Username and password don't match
 */
router.post('/login', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    const response = User.users.filter(
        (user) => user.username === username && user.password === password
    );

    if (response.length > 0) {
        res.status(200).json(
            {
                status: true,
                msg: "Login exitoso."
            }
        )
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "Error de credenciales."
            }
        )
    }
});


/**
 * @swagger
 *  /user/{id}:
 *  put:
 *      summary: Update user by id
 *      tags: [User]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: string
 *              required: true
 *              description: User's id
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                     $ref: '#/components/schemas/User'
 *      responses:
 *          200:
 *              description: The user was updated
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/User'
 *          401:
 *              description: User couldn't be updated
 *          
 */
router.put('/:id', (req, res) => {
    const { id } = req.params;
    const update_fields = req.body;
    const user = User.updateUser(id, update_fields);
    return res.json(user)
})


/**
 * @swagger
 * /user/{id}:
 *  delete:
 *      summary: Delete single user by id
 *      tags: [User]
 *      parameters:
 *          - in: path
 *            name: id
 *            schema:
 *              type: integer
 *              required: true
 *              description: User's id
 *      responses:
 *          200:
 *              description: User was successfully deleted
 *          401:
 *              description: User couldn't be deleted
 */
router.delete('/:id', (req, res) => {
    const { id } = req.params;
    User.deleteUser(id);

    const user = User.getUserById(id);

    if (user) {
        res.status(200).json(
            {
                status: true,
                msg: "El usuario ha sido eliminado."
            }
        );
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "No existe un usuario con ese id."
            }
        );
    }
    
});


/**
 * @swagger
 * tags:
 *  name: User
 *  description: User CRUD, signup, login.
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          required: 
 *              - username
 *              - password
 *              - mail
 *          properties:
 *              id:
 *                  type: integer
 *                  description: User's id
 *              username:
 *                  type: string
 *                  description: User's name
 *              password:
 *                  type: string
 *                  description: User's password
 *              fullName:
 *                  type: string
 *                  description: User's name and last name
 *              mail:
 *                  type: string
 *                  description: User's email
 *              phone:
 *                 type: string
 *                 description: User's phone number
 *              address:
 *                 type: string
 *                 description: User's address
 *              isAdmin:
 *                  type: boolean
 *                  description: True if user is amdin
 *          example:
 *              id: 1
 *              username: el_michi
 *              password: splinter123
 *              fullname: Splinter Fern
 *              mail: splinter@gmail.com
 *              phone: +54 911 6589 2563
 *              address: Manta Azul 2345
 *              isAdmin: true
 *   
 */

module.exports = router;