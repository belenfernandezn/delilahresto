const { json } = require('express');
const express = require('express');
const router = express.Router();
const Payment = require('../models/payment.model');

/**
 * @swagger
 * /payment:
 *  get:
 *      summary: Get all payment methods
 *      tags: [Payment Methods]
 *      responses:
 *          200:
 *              description: All payment methods
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Payment'
 *          401:
 *              description: User must be admin to get this info.
 */
router.get('/', (req, res) => {
    payment_methods = Payment.findAll();
    return res.json(payment_methods);
});

/**
 * @swagger
 * /payment:
 *  post:
 *      summary: Create new payment method
 *      tags: [Payment Methods]
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                      $ref: '#/components/schemas/Payment'
 *      responses:
 *          200:
 *              description: New payment method was created
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Payment'
 *          401:
 *              description: User must be admin to create new payment method.
 */
router.post('/', (req, res) => {
    const payment_method = req.body;
    new_payment_method = Payment.createNewPaymentMethod(payment_method);

    const id = payment_method.id;
    const payment_method_exists = Payment.getPaymentMethodById(id);

    if (payment_method_exists) {
        res.status(200).json(new_payment_method);
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "Ocurrió un error. El método de pago no ha sido creado."
            }
        );
    } 
});

/**
 * @swagger
 *  /payment/{id}:
 *  put:
 *      summary: Update payment method by id
 *      tags: [Payment Methods]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: string
 *              required: true
 *              description: Payment method's id
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                     $ref: '#/components/schemas/Payment'
 *      responses:
 *          200:
 *              description: Payment method was updated
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Payment'
 *          401:
 *              description: Payment method couldn't be updated. User must be admin.
 *          
 */
router.put('/:id', (req, res) => {
    const { id } = req.params;
    const update_fields = req.body;
    const payment_method = Payment.updatePaymentMethod(id, update_fields);
    return res.json(payment_method)
});

/**
 * @swagger
 * /payment/{id}:
 *  delete:
 *      summary: Delete single payment method by id
 *      tags: [Payment Methods]
 *      parameters:
 *          - in: path
 *            name: id
 *            schema:
 *              type: integer
 *              required: true
 *              description: Payment method's id
 *      responses:
 *          200:
 *              description: Payment method was successfully deleted
 *          401:
 *              description: Payment method couldn't be deleted. User must be admin.
 */
router.delete('/:id', (req, res) => {
    const { id } = req.params;
    Payment.deletePaymentMethod(id);

    const payment_method = Payment.getPaymentMethodById(id);

    if (payment_method) {
        res.status(200).json(
            {
                status: true,
                msg: "El metodo de pago ha sido eliminado."
            }
        );
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "No existe un metodo de pago con ese id."
            }
        );
    }
});


/**
 * @swagger
 * tags:
 *  name: Payment Methods
 *  description: Payment methods CRUD.
 * components:
 *  schemas:
 *      Payment:
 *          type: object
 *          required: 
 *              - name
 *          properties:
 *              id:
 *                  type: integer
 *                  description: Payment Method's id
 *              name:
 *                  type: string
 *                  description: Type of payment method
 *                  enum:
 *                  - Tarjeta de crédito
 *                  - Tarjeta de débito
 *                  - Efectivo
 *                  - Mercado Pago
 *                  - Criptomoneda
 *              bank:
 *                  type: string
 *                  description: Bank's name
 *          example:
 *              id: 1
 *              name: Tarjeta de crédito
 *              bank: Galicia
 *   
 */

module.exports = router;