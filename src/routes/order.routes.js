const express = require('express');
const router = express.Router();
const Order = require('../models/order.model');

const basicAuth = require('express-basic-auth');
const isAdministrator = require('../middlewares/check-admin-middleware');

/**
 * @swagger
 * /orders/all:
 *  get:
 *      summary: Get all orders
 *      tags: [Orders]
 *      responses:
 *          200:
 *              description: All orders' info
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Order'
 *          401:
 *              description: User must be admin to get this info
 */
router.get('/all', basicAuth( { authorizer: isAdministrator } ), (req, res) => {
    const orders = Order.findAll();
    return res.json(orders);
});


/**
 * @swagger
 * /orders/{username}:
 *  get:
 *      summary: Get customer's order history by username
 *      tags: [Orders]
 *      parameters:
 *          - in: path
 *            name: username
 *            schema:
 *              type: string
 *              required: true
 *              description: Customer's username
 *      responses:
 *          200:
 *              description: Customer's order history
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Order'
 *          401:
 *              description: Request failed
 */
 router.get('/:username', (req, res) => {
    const { username } = req.params;
    const orders = Order.findOrdersByUsername(username);
    return res.json(orders);
});


/**
 * @swagger
 * /orders:
 *  post:
 *      summary: Create new order
 *      tags: [Orders]
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                      $ref: '#/components/schemas/Order'
 *      responses:
 *          200:
 *              description: Order info
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Order'
 *          401:
 *              description: Order couldn't be created.
 */
 router.post('/', (req, res) => {
    const new_order = req.body;
    const order = Order.createNewOrder(new_order)

    const id = order.id;
    const order_exists = Order.getOrderById(id);

    if (order_exists) {
        res.status(200).json(order);
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "Ocurrió un error. La orden no ha sido creado."
            }
        );
    } 
});


/**
 * @swagger
 *  /orders/{id}:
 *  put:
 *      summary: Update order by id
 *      tags: [Orders]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: string
 *              required: true
 *              description: Order's id
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                     $ref: '#/components/schemas/Order'
 *      responses:
 *          200:
 *              description: The order was updated
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Order'
 *          401:
 *              description: Order couldn't be updated. User must be admin.
 *          
 */
router.put('/:id', basicAuth( { authorizer: isAdministrator } ), (req, res) => {
    const { id } = req.params;
    const status_value = req.body.status;
    const order = Order.updateOrderStatus(id, status_value);
    return res.json(order);
});



/**
 * @swagger
 * tags:
 *  name: Orders
 *  description: Order CRUD.
 * components:
 *  schemas:
 *      Order:
 *          type: object
 *          required: 
 *              - status
 *              - number
 *              - payment
 *              - username
 *              - address
 *          properties:
 *              id:
 *                  type: integer
 *                  description: Order's id
 *              status:
 *                  type: string
 *                  description: Order status
 *                  enum:
 *                  - Pendiente
 *                  - Confirmado
 *                  - En preparación
 *                  - Enviado
 *                  - Entregado
 *              time:
 *                  type: date-time
 *                  description: Time of ordering
 *              orderNumber:
 *                  type: string
 *                  description: Order number
 *              products:
 *                  type: array
 *                  description: Ordered product's id
 *              description:
 *                 type: string
 *                 description: Amount of each product and its name
 *              payment:
 *                 type: integer
 *                 description: Total amount that customer has to pay
 *              username:
 *                  type: sting
 *                  description: Username of customer who placed the order
 *              address:
 *                  type: string
 *                  description: Customer's address
 *          example:
 *              id: 1
 *              status: Confirmado
 *              time: 15:30
 *              orderNumber: 45
 *              products: [3, 5, 5]
 *              description: 1xSandwich Veggie 2xFocaccia
 *              payment: 910
 *              username: el_michi
 *              address: Manta Azul 2345
 *   
 */

module.exports = router;