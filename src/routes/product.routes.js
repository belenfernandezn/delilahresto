const express = require('express');
const router = express.Router();
const Product = require('../models/product.model');

const basicAuth = require('express-basic-auth');
const isAdministrator = require('../middlewares/check-admin-middleware');


/**
 * @swagger
 * /products:
 *  get:
 *      summary: Get all products
 *      tags: [Products]
 *      responses:
 *          200:
 *              description: All products
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Product'
 *          401:
 *              description: Couldn't get all products.
 */
router.get('/all', (req, res) => {
    const products = Product.findAll();
    return res.json(products);
});

/**
 * @swagger
 * /products:
 *  post:
 *      summary: Create new product
 *      tags: [Products]
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                      $ref: '#/components/schemas/Product'
 *      responses:
 *          200:
 *              description: New product was created
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Product'
 *          401:
 *              description: Product couldn't be created. User must be admin.
 */
router.post('/', basicAuth( { authorizer: isAdministrator } ), (req,res) => {
    const product = req.body;
    new_product = Product.createNewProduct(product);

    const id = product.id;
    const product_exists = Product.getProductById(id);

    if (product_exists) {
        res.status(200).json(new_product);
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "Ocurrió un error. El producto no ha sido creado."
            }
        );
    } 
});

/**
 * @swagger
 *  /products/{id}:
 *  put:
 *      summary: Update product by id
 *      tags: [Products]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: string
 *              required: true
 *              description: Product's id
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json:
 *                 schema:
 *                     $ref: '#/components/schemas/Product'
 *      responses:
 *          200:
 *              description: Product was updated
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Product'
 *          401:
 *              description: Product couldn't be updated. User must be admin.
 *          
 */
router.put('/:id', basicAuth( { authorizer: isAdministrator } ), (req, res) => {
    const { id } = req.params;
    const update_fields = req.body;
    const product = Product.updateProduct(id, update_fields);
    return res.json(product)
})

/**
 * @swagger
 * /products/{id}:
 *  delete:
 *      summary: Delete single product by id
 *      tags: [Products]
 *      parameters:
 *          - in: path
 *            name: id
 *            schema:
 *              type: integer
 *              required: true
 *              description: Product's id
 *      responses:
 *          200:
 *              description: Product was successfully deleted
 *          401:
 *              description: Product couldn't be deleted. User must be admin.
 */
router.delete('/:id', basicAuth( { authorizer: isAdministrator } ), (req, res) => {
    const { id } = req.params;
    Product.deleteProduct(id);

    const product = Product.getProductById(id);

    if (product) {
        res.status(200).json(
            {
                status: true,
                msg: "El producto ha sido eliminado."
            }
        );
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "No existe un producto con ese id."
            }
        );
    }
    
});


/**
 * @swagger
 * tags:
 *  name: Products
 *  description: Products CRUD.
 * components:
 *  schemas:
 *      Product:
 *          type: object
 *          required: 
 *              - name
 *              - price
 *          properties:
 *              id:
 *                  type: integer
 *                  description: Product's id
 *              nombre:
 *                  type: string
 *                  description: Product's name
 *              precio:
 *                  type: float
 *                  description: Product's price
 *          example:
 *              id: 1
 *              nombre: Hamburguesa clásica
 *              precio: 350
 *   
 */


module.exports = router;