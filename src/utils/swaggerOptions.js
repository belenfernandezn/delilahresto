const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Delilah Restó",
            version: "1.0.0",
            description: "Sprint Project de Acámica Backend"
        },
        servers: [
            {
                url: 'http://localhost:3000',
                description: "Local server"
            },
            {
                url: 'http://development.com:3000',
                description: "Dev server"
            }
        ],
        components: {
            securitySchemes: {
                basicAuth: {
                    type: "http",
                    scheme: "basic"
                }
            }
        },
        security: [
            {
                basicAuth: []
            }
        ]
    },
    apis: ["./src/routes/*.js"]
}

module.exports = swaggerOptions;