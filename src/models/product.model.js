const products = [
    {
        id: 1,
        nombre: "Bagel de salmón",
        precio: 425
    },
    {
        id: 2,
        nombre: "Hamburguesa clásica",
        precio: 350
    },
    {
        id: 3,
        nombre: "Sandwich veggie",
        precio: 310
    },
    {
        id: 4,
        nombre: "Ensalada veggie",
        precio: 340
    },
    {
        id: 5,
        nombre: "Focaccia",
        precio: 300
    },
    {
        id: 6,
        nombre: "Sandwich Focaccia",
        precio: 440
    }
]


const findAll = () => {
    return products;
};


const getProductById = (id) => {
    id = parseInt(id);
    const product = products.filter(product => product.id === id);
    return product;
};


const getProductIndexById = (id) => {
    const productIndex = products.findIndex(product => product.id === id);
    return productIndex;
};


const updateProduct = (id, fields) => {
    id = parseInt(id);
    const productIndex = getProductIndexById(id);

    for (const [key, value] of Object.entries(fields)) {
        products[productIndex][key] = value;
    }
    return products[productIndex];
};


const deleteProduct = (id) => {
    id = parseInt(id);
    productIndex = getProductIndexById(id);
    products.splice(productIndex, 1);
};


const createNewProduct = (product) => {
    products.push(product);
    return product;
};


module.exports = { findAll, getProductById, updateProduct, deleteProduct, createNewProduct }