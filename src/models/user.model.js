const users = [
    {
        id: 1,
        username: 'el_michi',
        password: 'splinter123',
        fullName: 'Splinter Fern',
        mail: 'el_michi@gmail.com',
        phone: '+54 911 6589 2563',
        address: 'Manta Azul 2345',
        isAdmin: true,
    },
    {
        id: 2,
        username: 'la_gati',
        password: 'nala123',
        fullName: 'Nala Marq',
        mail: 'la_gati@gmail.com',
        phone: '+54 911 2365 7891',
        address: 'Silla Negra 4589',
        isAdmin: false,
    }
]


const getUserIndexById = (id) => {
    const userIndex = users.findIndex(user => user.id === id);
    return userIndex;
}


const getUserById = (id) => {
    id = parseInt(id);
    const user = users.filter(user => user.id === id);
    return user;
}


const findAll = () => {
    return users;
}


const createNewUser = (user) => {
    users.push(user);
    return user
}


const updateUser = (id, fields) => {
    id = parseInt(id)
    const userIndex = getUserIndexById(id);

    for (const [key, value] of Object.entries(fields)) {
        users[userIndex][key] = value;
    }
    return users[userIndex];
}


const deleteUser = (id) => {
    id = parseInt(id)
    userIndex = getUserIndexById(id);
    users.splice(userIndex, 1);
}



module.exports = { users, findAll, createNewUser, deleteUser, updateUser, getUserById }