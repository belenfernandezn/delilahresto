payment_methods = [
    {
        id: 1,
        name: "Tarjeta de credito",
        bank: "Santander"
    },
    {
        id: 2,
        name: "Efectivo",
        bank: ""
    },
    {
        id: 3,
        name: "Tarjeta de debito",
        bank: "Galicia"
    }
];


const findAll = () => {
    return payment_methods;
};


const createNewPaymentMethod = (payment_method) => {
    payment_methods.push(payment_method);
    return payment_method;
};


const getPaymentMethodById = (id) => {
    id = parseInt(id);
    const payment_method = payment_methods.filter(payment_method => payment_method.id === id);
    return payment_method;
};


const getPaymentMethodIndexById = (id) => {
    const paymentIndex = payment_methods.findIndex(payment_method => payment_method.id === id);
    return paymentIndex;
};


const updatePaymentMethod = (id, fields) => {
    id = parseInt(id);
    const paymenttIndex = getPaymentMethodIndexById(id);

    for (const [key, value] of Object.entries(fields)) {
        payment_methods[paymenttIndex][key] = value;
    }
    return payment_methods[paymenttIndex];
};


const deletePaymentMethod = (id) => {
    id = parseInt(id);
    paymentIndex = getPaymentMethodIndexById(id);
    payment_methods.splice(paymentIndex, 1);
};



module.exports = {payment_methods, findAll, createNewPaymentMethod, getPaymentMethodById, updatePaymentMethod, deletePaymentMethod}