orders = [
    {
        id: 1,
        status: "Confirmado",
        time: "15:30",
        orderNumber: "45",
        products: [3, 5, 5],
        description: "1xSandwich Veggie 2xFocaccia",
        payment: 910.0,
        username: "el_michi",
        address: "Manta Azul 2345"
    },
    {
        id: 2,
        status: "En preparacion",
        time: "15:45",
        orderNumber: "45",
        products: [3, 5, 5],
        description: "1xSandwich Veggie 2xFocaccia",
        payment: 910.0,
        username: "el_michi",
        address: "Manta Azul 2345"
    },
    {
        id: 3,
        status: "Enviado",
        time: "15:45",
        orderNumber: "45",
        products: [3, 5, 5],
        description: "1xSandwich Veggie 2xFocaccia",
        payment: 910.0,
        username: "la_gati",
        address: "Silla Negra 4589"
    }
]


const findAll = () => {
    return orders;
};


const getOrderIndexById = (id) => {
    const orderIndex = orders.findIndex(order => order.id === id);
    return orderIndex;
};


const updateOrderStatus = (id, new_value) => {
    id = parseInt(id);
    const orderIndex = getOrderIndexById(id);

    orders[orderIndex]['status'] = new_value

    return orders[orderIndex];
};


const getOrderById = (id) => {
    id = parseInt(id);
    const order = orders.filter(order => order.id === id);
    return order;
};


const createNewOrder = (order) => {
    orders.push(order);
    return order;
};


const findOrdersByUsername = (username) => {
    const order_list = orders.filter(order => order.username === username)
    console.log(order_list);

    return order_list;
};


module.exports = {orders, findAll, updateOrderStatus, getOrderById, createNewOrder, findOrdersByUsername}