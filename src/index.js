const basicAuth = require('express-basic-auth');
const express = require('express');

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const userRoutes = require('./routes/user.routes');
const productRoutes = require('./routes/product.routes');
const orderRoutes = require('./routes/order.routes');
const paymentRoutes = require('./routes/payment.routes');
const User = require('./models/user.model');

const myCustomAuthorizer = require('./middlewares/basic-auth-middleware');
const isAdministrator = require('./middlewares/check-admin-middleware');
const swaggerOptions = require('./utils/swaggerOptions')

const app = express();
app.use(express.json());


const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs));

app.use(basicAuth({ authorizer: myCustomAuthorizer }));

app.use('/user', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

app.use(basicAuth( { authorizer: isAdministrator } ));
app.use('/payment', paymentRoutes);


app.listen(3000, () => { console.log("server listening on port 3000") });