# Sprint 1 - Delilah Restó

This is an API for a Restaurant's management.
Express project for Acamica's Backend Developer Course.


## How it works

You can check out the documentation on:
```
http://127.0.0.1:3000/api-docs
```

## Getting Started

These instructions will help you run the program in your terminal.

### Installing dependencies

From the root directory run the following command:

```
npm install
```

### Running the program

From the root directory run the following command:

```
npm start
```

### Some endpoints require you to be an admin user

```
{
    username: el_michi
    password: splinter123
}
```
